-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2019 at 11:24 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snipeit`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessories`
--

CREATE TABLE `accessories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `requestable` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(20,2) DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `min_amt` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `model_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `accessories_users`
--

CREATE TABLE `accessories_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `accessory_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_logs`
--

CREATE TABLE `action_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(11) DEFAULT NULL,
  `target_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `filename` text COLLATE utf8_unicode_ci,
  `item_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `expected_checkin` date DEFAULT NULL,
  `accepted_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `accept_signature` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(20,2) DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `image` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `physical` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT NULL,
  `warranty_months` int(11) DEFAULT NULL,
  `depreciate` tinyint(1) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `requestable` tinyint(4) NOT NULL DEFAULT '0',
  `rtd_location_id` int(11) DEFAULT NULL,
  `_snipeit_mac_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accepted` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_checkout` datetime DEFAULT NULL,
  `expected_checkin` date DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `_snipeit_memory_notebook` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_logs`
--

CREATE TABLE `asset_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) NOT NULL,
  `checkedout_to` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `asset_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `filename` text COLLATE utf8_unicode_ci,
  `requested_at` datetime DEFAULT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `accessory_id` int(11) DEFAULT NULL,
  `accepted_id` int(11) DEFAULT NULL,
  `consumable_id` int(11) DEFAULT NULL,
  `expected_checkin` date DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_maintenances`
--

CREATE TABLE `asset_maintenances` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `asset_maintenance_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_warranty` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `completion_date` date DEFAULT NULL,
  `asset_maintenance_time` int(11) DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `cost` decimal(20,2) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_uploads`
--

CREATE TABLE `asset_uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) NOT NULL,
  `filenotes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `eula_text` longtext COLLATE utf8_unicode_ci,
  `use_default_eula` tinyint(1) NOT NULL DEFAULT '0',
  `require_acceptance` tinyint(1) NOT NULL DEFAULT '0',
  `category_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'asset',
  `checkin_email` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `user_id`, `deleted_at`, `eula_text`, `use_default_eula`, `require_acceptance`, `category_type`, `checkin_email`) VALUES
(1, 'Laptop', '2019-03-28 10:20:18', '2019-03-28 10:20:18', 502, NULL, 'laptop baru', 0, 0, 'asset', 0);

-- --------------------------------------------------------

--
-- Table structure for table `checkout_requests`
--

CREATE TABLE `checkout_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `requestable_id` int(11) NOT NULL,
  `requestable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Docotel Teknologi', '2017-08-23 11:07:45', '2017-08-23 11:07:45'),
(2, 'Solusi Pembayaran Elektronik', '2017-08-24 09:16:21', '2017-08-24 09:16:21'),
(3, 'Pabrik Solusi', '2017-08-24 09:16:27', '2017-08-24 09:16:27'),
(5, 'PT ETFIN', '2018-07-19 09:06:51', '2018-07-19 09:06:51');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `min_amt` int(11) DEFAULT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `components_assets`
--

CREATE TABLE `components_assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `assigned_qty` int(11) DEFAULT '1',
  `component_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consumables`
--

CREATE TABLE `consumables` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `requestable` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(20,2) DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `min_amt` int(11) DEFAULT NULL,
  `model_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `item_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consumables_users`
--

CREATE TABLE `consumables_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `consumable_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `element` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `field_values` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_encrypted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `name`, `format`, `element`, `created_at`, `updated_at`, `user_id`, `field_values`, `field_encrypted`) VALUES
(1, 'MAC Address', 'regex:/^[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}$/', 'text', NULL, '2017-08-23 11:06:20', NULL, NULL, 0),
(3, 'Memory Notebook', '', 'text', '2017-09-11 04:40:21', '2017-09-11 04:40:21', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `custom_fieldsets`
--

CREATE TABLE `custom_fieldsets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_fieldsets`
--

INSERT INTO `custom_fieldsets` (`id`, `name`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Asset with MAC Address', NULL, NULL, NULL),
(2, 'Memory', '2017-09-11 04:14:17', '2017-09-11 04:14:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_custom_fieldset`
--

CREATE TABLE `custom_field_custom_fieldset` (
  `custom_field_id` int(11) NOT NULL,
  `custom_fieldset_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_field_custom_fieldset`
--

INSERT INTO `custom_field_custom_fieldset` (`custom_field_id`, `custom_fieldset_id`, `order`, `required`) VALUES
(1, 1, 1, 0),
(3, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `depreciations`
--

CREATE TABLE `depreciations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `months` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '{\"superuser\":\"1\",\"admin\":\"1\",\"reports.view\":\"1\",\"assets.view\":\"1\",\"assets.create\":\"1\",\"assets.edit\":\"1\",\"assets.delete\":\"1\",\"assets.checkin\":\"1\",\"assets.checkout\":\"1\",\"assets.view.requestable\":\"1\",\"accessories.view\":\"1\",\"accessories.create\":\"1\",\"accessories.edit\":\"1\",\"accessories.delete\":\"1\",\"accessories.checkout\":\"1\",\"accessories.checkin\":\"1\",\"consumables.view\":\"1\",\"consumables.create\":\"1\",\"consumables.edit\":\"1\",\"consumables.delete\":\"1\",\"consumables.checkout\":\"1\",\"licenses.view\":\"1\",\"licenses.create\":\"1\",\"licenses.edit\":\"1\",\"licenses.delete\":\"1\",\"licenses.checkout\":\"1\",\"licenses.keys\":\"1\",\"components.view\":\"1\",\"components.create\":\"1\",\"components.edit\":\"1\",\"components.delete\":\"1\",\"components.checkout\":\"1\",\"components.checkin\":\"1\",\"users.view\":\"1\",\"users.create\":\"1\",\"users.edit\":\"1\",\"users.delete\":\"1\",\"self.two_factor\":\"1\"}', '2017-08-24 09:06:43', '2017-08-24 09:06:43'),
(2, 'Asset Manager', '{\"superuser\":\"0\",\"admin\":\"0\",\"reports.view\":\"1\",\"assets.view\":\"1\",\"assets.create\":\"1\",\"assets.edit\":\"1\",\"assets.delete\":\"1\",\"assets.checkin\":\"1\",\"assets.checkout\":\"1\",\"assets.view.requestable\":\"1\",\"accessories.view\":\"1\",\"accessories.create\":\"1\",\"accessories.edit\":\"1\",\"accessories.delete\":\"1\",\"accessories.checkout\":\"1\",\"accessories.checkin\":\"1\",\"consumables.view\":\"1\",\"consumables.create\":\"1\",\"consumables.edit\":\"1\",\"consumables.delete\":\"1\",\"consumables.checkout\":\"1\",\"licenses.view\":\"1\",\"licenses.create\":\"1\",\"licenses.edit\":\"1\",\"licenses.delete\":\"1\",\"licenses.checkout\":\"1\",\"licenses.keys\":\"1\",\"components.view\":\"1\",\"components.create\":\"1\",\"components.edit\":\"1\",\"components.delete\":\"1\",\"components.checkout\":\"1\",\"components.checkin\":\"1\",\"users.view\":\"0\",\"users.create\":\"0\",\"users.edit\":\"0\",\"users.delete\":\"0\",\"self.two_factor\":\"1\"}', '2017-08-24 09:09:52', '2017-08-24 09:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_cost` decimal(20,2) DEFAULT NULL,
  `order_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seats` int(11) NOT NULL DEFAULT '1',
  `notes` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `depreciation_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `license_name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depreciate` tinyint(1) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `purchase_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termination_date` date DEFAULT NULL,
  `maintained` tinyint(1) DEFAULT NULL,
  `reassignable` tinyint(1) NOT NULL DEFAULT '1',
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `license_seats`
--

CREATE TABLE `license_seats` (
  `id` int(10) UNSIGNED NOT NULL,
  `license_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `address` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `city`, `state`, `country`, `created_at`, `updated_at`, `user_id`, `address`, `address2`, `zip`, `deleted_at`, `parent_id`, `currency`) VALUES
(1, 'Joglo', '', '', '', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, '', NULL, NULL, NULL, NULL, NULL),
(2, 'Rambai', '', '', '', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, '', NULL, NULL, NULL, NULL, NULL),
(3, 'Kerinci', '', '', '', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, '', NULL, NULL, NULL, NULL, NULL),
(4, 'ITC Permata Hijau', '', '', '', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, '', NULL, NULL, NULL, NULL, NULL),
(5, 'Yogyakarta', '', '', 'ID', '2017-09-11 03:17:51', '2017-09-11 03:17:51', 1, '', '', '', NULL, NULL, ''),
(6, 'Bandung', '', '', 'ID', '2017-09-11 03:18:05', '2017-09-11 03:18:16', 1, '', '', '', NULL, NULL, ''),
(7, 'Makasar', 'Makasar', '', 'ID', '2017-09-30 10:05:04', '2017-09-30 10:05:04', 165, '', NULL, NULL, NULL, NULL, 'Rp'),
(8, 'Docotel World Lantai 5', 'Jakarta', '', 'ID', '2017-12-05 06:28:36', '2018-04-04 04:26:02', 375, '', '', '', NULL, NULL, 'Rp'),
(9, 'DCI', 'Bekasi', '', '', '2018-03-06 03:35:18', '2018-03-06 03:35:18', 165, '', NULL, NULL, NULL, NULL, 'Rp'),
(10, 'Docotel World Lantai 6', 'Roxy', '', 'ID', '2018-04-02 10:02:44', '2018-04-04 04:26:20', 388, '', '', '', NULL, NULL, 'Rp'),
(11, 'Roxy lantai 1', 'jakarta', '', 'ID', '2018-04-04 08:38:46', '2018-04-04 08:38:46', 388, '', NULL, NULL, NULL, NULL, 'Rp'),
(12, 'Docotel World Lantai 1', 'roxy', '', 'ID', '2018-04-04 08:39:52', '2018-04-04 08:39:52', 388, '', NULL, NULL, NULL, NULL, 'Rp'),
(13, 'Docotel World Lantai 3', 'Jakarta', '', '', '2018-04-04 10:19:40', '2018-04-04 10:19:40', 389, '', NULL, NULL, NULL, NULL, 'Rp'),
(14, 'Docotel World Lantai 2', 'jakarta', '', 'ID', '2018-04-04 10:43:10', '2018-04-04 10:43:10', 388, '', NULL, NULL, NULL, NULL, 'Rp'),
(15, 'Kantin', 'Docotel World Lantai 7', '', 'ID', '2018-08-06 09:40:43', '2018-08-06 09:40:43', 390, '', NULL, NULL, NULL, NULL, 'Rp'),
(16, 'Docotel World Lantai 7 (Kantin)', 'Jakarta', '', 'ID', '2018-08-06 09:41:33', '2018-08-06 09:41:33', 390, '', NULL, NULL, NULL, NULL, 'Rp'),
(17, 'Docotel World Basement', 'Jakarta Pusat', '', 'ID', '2018-08-09 06:31:38', '2018-08-09 06:31:38', 390, '', NULL, NULL, NULL, NULL, 'Rp');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `created_at`, `updated_at`, `user_id`, `deleted_at`) VALUES
(1, 'BenQ', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(2, 'Lenovo', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(3, 'LG', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(4, 'Apple', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(5, 'Dell', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(6, 'Samsung', '2017-08-25 15:06:02', '2017-08-25 15:06:02', 1, NULL),
(7, 'Asus', '2017-08-25 15:06:03', '2017-08-25 15:06:03', 1, NULL),
(8, 'Fujitsu', '2017-08-25 15:06:03', '2017-08-25 15:06:03', 1, NULL),
(9, 'Logitech', '2017-09-11 05:28:22', '2017-09-11 05:28:22', 292, NULL),
(10, 'HP (Hewlett Pakard)', '2017-09-11 07:12:56', '2017-09-11 07:12:56', 292, NULL),
(11, 'Mikrotik', '2017-09-11 07:26:20', '2017-09-11 07:26:20', 292, NULL),
(12, 'Linksys', '2017-09-11 07:38:49', '2017-09-11 07:38:49', 292, NULL),
(13, 'Ubiquity', '2017-09-11 08:02:22', '2017-09-11 08:02:22', 292, NULL),
(14, 'Panasonic', '2017-09-12 04:25:28', '2017-09-12 04:25:28', 292, NULL),
(15, 'PC-Rakitan', '2017-09-12 06:28:29', '2017-09-12 06:28:29', 292, NULL),
(16, 'Solution', '2017-09-12 06:35:41', '2017-09-12 06:35:41', 292, NULL),
(17, 'Epson', '2017-09-13 02:57:57', '2017-09-13 02:57:57', 293, NULL),
(18, 'APC', '2017-09-13 06:43:14', '2017-09-13 06:43:14', 293, NULL),
(19, 'Krisbrow', '2017-09-13 06:54:08', '2017-09-13 06:54:08', 293, NULL),
(20, 'Advance', '2017-09-13 07:27:54', '2017-09-13 07:27:54', 292, NULL),
(21, 'D-Link', '2017-09-13 08:02:22', '2017-09-13 08:02:22', 293, NULL),
(22, 'SanDisk', '2017-09-13 08:22:37', '2017-09-13 08:22:37', 293, NULL),
(23, 'Western Digital', '2017-09-14 08:56:37', '2017-09-14 08:56:37', 292, NULL),
(24, 'MOVIMAX', '2017-09-18 10:32:43', '2017-09-18 10:32:43', 293, NULL),
(25, 'Toshiba', '2017-09-19 07:43:09', '2017-09-19 07:43:09', 292, NULL),
(26, 'Votre', '2017-09-29 06:28:14', '2017-09-29 06:28:14', 293, NULL),
(27, 'Juniper', '2017-12-05 06:08:24', '2017-12-05 06:08:24', 375, NULL),
(28, 'Kema-K', '2017-12-18 08:48:38', '2017-12-18 08:48:38', 375, NULL),
(29, 'V-GEN', '2017-12-18 10:01:27', '2017-12-18 10:01:27', 375, NULL),
(30, 'Palo Alto', '2018-03-23 06:58:48', '2018-03-23 06:58:48', 383, NULL),
(31, 'Zyxel', '2018-05-08 04:23:51', '2018-05-08 04:23:51', 165, NULL),
(32, 'Optoma', '2018-05-08 09:55:18', '2018-05-08 09:55:18', 293, NULL),
(33, 'Xiaomi', '2018-05-14 07:18:24', '2018-05-14 07:18:24', 165, NULL),
(34, 'Advan', '2018-05-14 07:28:34', '2018-05-14 07:28:34', 165, NULL),
(35, 'Infokus', '2018-05-14 08:31:27', '2018-05-14 08:31:27', 165, NULL),
(36, 'Nexus', '2018-05-17 02:12:26', '2018-05-17 02:12:26', 165, NULL),
(37, '3com', '2018-05-22 06:54:09', '2018-05-22 06:54:09', 165, NULL),
(38, 'cisco', '2018-05-22 06:54:34', '2018-05-22 06:54:34', 165, NULL),
(39, 'tp-link', '2018-05-22 07:36:06', '2018-05-22 07:36:06', 165, NULL),
(40, 'Bolt', '2018-05-23 07:06:07', '2018-05-23 07:06:07', 165, NULL),
(41, 'google', '2018-05-28 02:55:43', '2018-05-28 02:55:43', 165, NULL),
(42, 'vga to hdmi', '2018-05-28 06:15:31', '2018-05-28 06:15:31', 165, NULL),
(43, 'usenda', '2018-06-11 04:21:45', '2018-06-11 04:21:45', 165, NULL),
(44, 'Nokia', '2018-06-11 08:34:44', '2018-06-11 08:34:44', 293, NULL),
(45, 'Motorola', '2018-09-12 09:48:08', '2018-09-12 09:48:08', 293, NULL),
(46, 'Sony', '2018-09-12 09:48:25', '2018-09-12 09:48:25', 293, NULL),
(47, 'Vivo', '2018-09-12 09:48:46', '2018-09-12 09:48:46', 293, NULL),
(48, 'HikVision', '2018-12-04 03:06:59', '2018-12-04 03:06:59', 293, NULL),
(49, 'Huawei', '2019-01-24 04:29:22', '2019-01-24 04:29:22', 383, NULL),
(50, 'VMWare', '2019-02-04 04:34:52', '2019-02-04 04:34:52', 222, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2013_03_23_193214_update_users_table', 1),
('2013_11_13_075318_create_models_table', 1),
('2013_11_13_075335_create_categories_table', 1),
('2013_11_13_075347_create_manufacturers_table', 1),
('2013_11_15_015858_add_user_id_to_categories', 1),
('2013_11_15_112701_add_user_id_to_manufacturers', 1),
('2013_11_15_190327_create_assets_table', 1),
('2013_11_15_190357_create_licenses_table', 1),
('2013_11_15_201848_add_license_name_to_licenses', 1),
('2013_11_16_040323_create_depreciations_table', 1),
('2013_11_16_042851_add_depreciation_id_to_models', 1),
('2013_11_16_084923_add_user_id_to_models', 1),
('2013_11_16_103258_create_locations_table', 1),
('2013_11_16_103336_add_location_id_to_assets', 1),
('2013_11_16_103407_add_checkedout_to_to_assets', 1),
('2013_11_16_103425_create_history_table', 1),
('2013_11_17_054359_drop_licenses_table', 1),
('2013_11_17_054526_add_physical_to_assets', 1),
('2013_11_17_055126_create_settings_table', 1),
('2013_11_17_062634_add_license_to_assets', 1),
('2013_11_18_134332_add_contacts_to_users', 1),
('2013_11_18_142847_add_info_to_locations', 1),
('2013_11_18_152942_remove_location_id_from_asset', 1),
('2013_11_18_164423_set_nullvalues_for_user', 1),
('2013_11_19_013337_create_asset_logs_table', 1),
('2013_11_19_061409_edit_added_on_asset_logs_table', 1),
('2013_11_19_062250_edit_location_id_asset_logs_table', 1),
('2013_11_20_055822_add_soft_delete_on_assets', 1),
('2013_11_20_121404_add_soft_delete_on_locations', 1),
('2013_11_20_123137_add_soft_delete_on_manufacturers', 1),
('2013_11_20_123725_add_soft_delete_on_categories', 1),
('2013_11_20_130248_create_status_labels', 1),
('2013_11_20_130830_add_status_id_on_assets_table', 1),
('2013_11_20_131544_add_status_type_on_status_labels', 1),
('2013_11_20_134103_add_archived_to_assets', 1),
('2013_11_21_002321_add_uploads_table', 1),
('2013_11_21_024531_remove_deployable_boolean_from_status_labels', 1),
('2013_11_22_075308_add_option_label_to_settings_table', 1),
('2013_11_22_213400_edits_to_settings_table', 1),
('2013_11_25_013244_create_licenses_table', 1),
('2013_11_25_031458_create_license_seats_table', 1),
('2013_11_25_032022_add_type_to_actionlog_table', 1),
('2013_11_25_033008_delete_bad_licenses_table', 1),
('2013_11_25_033131_create_new_licenses_table', 1),
('2013_11_25_033534_add_licensed_to_licenses_table', 1),
('2013_11_25_101308_add_warrantee_to_assets_table', 1),
('2013_11_25_104343_alter_warranty_column_on_assets', 1),
('2013_11_25_150450_drop_parent_from_categories', 1),
('2013_11_25_151920_add_depreciate_to_assets', 1),
('2013_11_25_152903_add_depreciate_to_licenses_table', 1),
('2013_11_26_211820_drop_license_from_assets_table', 1),
('2013_11_27_062510_add_note_to_asset_logs_table', 1),
('2013_12_01_113426_add_filename_to_asset_log', 1),
('2013_12_06_094618_add_nullable_to_licenses_table', 1),
('2013_12_10_084038_add_eol_on_models_table', 1),
('2013_12_12_055218_add_manager_to_users_table', 1),
('2014_01_28_031200_add_qr_code_to_settings_table', 1),
('2014_02_13_183016_add_qr_text_to_settings_table', 1),
('2014_05_24_093839_alter_default_license_depreciation_id', 1),
('2014_05_27_231658_alter_default_values_licenses', 1),
('2014_06_19_191508_add_asset_name_to_settings', 1),
('2014_06_20_004847_make_asset_log_checkedout_to_nullable', 1),
('2014_06_20_005050_make_asset_log_purchasedate_to_nullable', 1),
('2014_06_24_003011_add_suppliers', 1),
('2014_06_24_010742_add_supplier_id_to_asset', 1),
('2014_06_24_012839_add_zip_to_supplier', 1),
('2014_06_24_033908_add_url_to_supplier', 1),
('2014_07_08_054116_add_employee_id_to_users', 1),
('2014_07_09_134316_add_requestable_to_assets', 1),
('2014_07_17_085822_add_asset_to_software', 1),
('2014_07_17_161625_make_asset_id_in_logs_nullable', 1),
('2014_08_12_053504_alpha_0_4_2_release', 1),
('2014_08_17_083523_make_location_id_nullable', 1),
('2014_10_16_200626_add_rtd_location_to_assets', 1),
('2014_10_24_000417_alter_supplier_state_to_32', 1),
('2014_10_24_015641_add_display_checkout_date', 1),
('2014_10_28_222654_add_avatar_field_to_users_table', 1),
('2014_10_29_045924_add_image_field_to_models_table', 1),
('2014_11_01_214955_add_eol_display_to_settings', 1),
('2014_11_04_231416_update_group_field_for_reporting', 1),
('2014_11_05_212408_add_fields_to_licenses', 1),
('2014_11_07_021042_add_image_to_supplier', 1),
('2014_11_20_203007_add_username_to_user', 1),
('2014_11_20_223947_add_auto_to_settings', 1),
('2014_11_20_224421_add_prefix_to_settings', 1),
('2014_11_21_104401_change_licence_type', 1),
('2014_12_09_082500_add_fields_maintained_term_to_licenses', 1),
('2015_02_04_155757_increase_user_field_lengths', 1),
('2015_02_07_013537_add_soft_deleted_to_log', 1),
('2015_02_10_040958_fix_bad_assigned_to_ids', 1),
('2015_02_10_053310_migrate_data_to_new_statuses', 1),
('2015_02_11_044104_migrate_make_license_assigned_null', 1),
('2015_02_11_104406_migrate_create_requests_table', 1),
('2015_02_12_001312_add_mac_address_to_asset', 1),
('2015_02_12_024100_change_license_notes_type', 1),
('2015_02_17_231020_add_localonly_to_settings', 1),
('2015_02_19_222322_add_logo_and_colors_to_settings', 1),
('2015_02_24_072043_add_alerts_to_settings', 1),
('2015_02_25_022931_add_eula_fields', 1),
('2015_02_25_204513_add_accessories_table', 1),
('2015_02_26_091228_add_accessories_user_table', 1),
('2015_02_26_115128_add_deleted_at_models', 1),
('2015_02_26_233005_add_category_type', 1),
('2015_03_01_231912_update_accepted_at_to_acceptance_id', 1),
('2015_03_05_011929_add_qr_type_to_settings', 1),
('2015_03_18_055327_add_note_to_user', 1),
('2015_04_29_234704_add_slack_to_settings', 1),
('2015_05_04_085151_add_parent_id_to_locations_table', 1),
('2015_05_22_124421_add_reassignable_to_licenses', 1),
('2015_06_10_003314_fix_default_for_user_notes', 1),
('2015_06_10_003554_create_consumables', 1),
('2015_06_15_183253_move_email_to_username', 1),
('2015_06_23_070346_make_email_nullable', 1),
('2015_06_26_213716_create_asset_maintenances_table', 1),
('2015_07_04_212443_create_custom_fields_table', 1),
('2015_07_09_014359_add_currency_to_settings_and_locations', 1),
('2015_07_21_122022_add_expected_checkin_date_to_asset_logs', 1),
('2015_07_24_093845_add_checkin_email_to_category_table', 1),
('2015_07_25_055415_remove_email_unique_constraint', 1),
('2015_07_29_230054_add_thread_id_to_asset_logs_table', 1),
('2015_07_31_015430_add_accepted_to_assets', 1),
('2015_09_09_195301_add_custom_css_to_settings', 1),
('2015_09_21_235926_create_custom_field_custom_fieldset', 1),
('2015_09_22_000104_create_custom_fieldsets', 1),
('2015_09_22_003321_add_fieldset_id_to_assets', 1),
('2015_09_22_003413_migrate_mac_address', 1),
('2015_09_28_003314_fix_default_purchase_order', 1),
('2015_10_01_024551_add_accessory_consumable_price_info', 1),
('2015_10_12_192706_add_brand_to_settings', 1),
('2015_10_22_003314_fix_defaults_accessories', 1),
('2015_10_23_182625_add_checkout_time_and_expected_checkout_date_to_assets', 1),
('2015_11_05_061015_create_companies_table', 1),
('2015_11_05_061115_add_company_id_to_consumables_table', 1),
('2015_11_05_183749_image', 1),
('2015_11_06_092038_add_company_id_to_accessories_table', 1),
('2015_11_06_100045_add_company_id_to_users_table', 1),
('2015_11_06_134742_add_company_id_to_licenses_table', 1),
('2015_11_08_035832_add_company_id_to_assets_table', 1),
('2015_11_08_222305_add_ldap_fields_to_settings', 1),
('2015_11_15_151803_add_full_multiple_companies_support_to_settings_table', 1),
('2015_11_26_195528_import_ldap_settings', 1),
('2015_11_30_191504_remove_fk_company_id', 1),
('2015_12_21_193006_add_ldap_server_cert_ignore_to_settings_table', 1),
('2015_12_30_233509_add_timestamp_and_userId_to_custom_fields', 1),
('2015_12_30_233658_add_timestamp_and_userId_to_custom_fieldsets', 1),
('2016_01_28_041048_add_notes_to_models', 1),
('2016_02_19_070119_add_remember_token_to_users_table', 1),
('2016_02_19_073625_create_password_resets_table', 1),
('2016_03_02_193043_add_ldap_flag_to_users', 1),
('2016_03_02_220517_update_ldap_filter_to_longer_field', 1),
('2016_03_08_225351_create_components_table', 1),
('2016_03_09_024038_add_min_stock_to_tables', 1),
('2016_03_10_133849_add_locale_to_users', 1),
('2016_03_10_135519_add_locale_to_settings', 1),
('2016_03_11_185621_add_label_settings_to_settings', 1),
('2016_03_22_125911_fix_custom_fields_regexes', 1),
('2016_04_28_141554_add_show_to_users', 1),
('2016_05_16_164733_add_model_mfg_to_consumable', 1),
('2016_05_19_180351_add_alt_barcode_settings', 1),
('2016_05_19_191146_add_alter_interval', 1),
('2016_05_19_192226_add_inventory_threshold', 1),
('2016_05_20_024859_remove_option_keys_from_settings_table', 1),
('2016_05_20_143758_remove_option_value_from_settings_table', 1),
('2016_06_01_140218_add_email_domain_and_format_to_settings', 1),
('2016_06_22_160725_add_user_id_to_maintenances', 1),
('2016_07_13_150015_add_is_ad_to_settings', 1),
('2016_07_14_153609_add_ad_domain_to_settings', 1),
('2016_07_22_003348_fix_custom_fields_regex_stuff', 1),
('2016_07_22_054850_one_more_mac_addr_fix', 1),
('2016_07_22_143045_add_port_to_ldap_settings', 1),
('2016_07_22_153432_add_tls_to_ldap_settings', 1),
('2016_07_27_211034_add_zerofill_to_settings', 1),
('2016_08_02_124944_add_color_to_statuslabel', 1),
('2016_08_04_134500_add_disallow_ldap_pw_sync_to_settings', 1),
('2016_08_09_002225_add_manufacturer_to_licenses', 1),
('2016_08_12_121613_add_manufacturer_to_accessories_table', 1),
('2016_08_23_143353_add_new_fields_to_custom_fields', 1),
('2016_08_23_145619_add_show_in_nav_to_status_labels', 1),
('2016_08_30_084634_make_purchase_cost_nullable', 1),
('2016_09_01_141051_add_requestable_to_asset_model', 1),
('2016_09_02_001448_create_checkout_requests_table', 1),
('2016_09_04_180400_create_actionlog_table', 1),
('2016_09_04_182149_migrate_asset_log_to_action_log', 1),
('2016_09_19_235935_fix_fieldtype_for_target_type', 1),
('2016_09_23_140722_fix_modelno_in_consumables_to_string', 1),
('2016_09_28_231359_add_company_to_logs', 1),
('2016_10_14_130709_fix_order_number_to_varchar', 1),
('2016_10_16_015024_rename_modelno_to_model_number', 1),
('2016_10_16_015211_rename_consumable_modelno_to_model_number', 1),
('2016_10_16_143235_rename_model_note_to_notes', 1),
('2016_10_16_165052_rename_component_total_qty_to_qty', 1),
('2016_10_19_145520_fix_order_number_in_components_to_string', 1),
('2016_10_27_151715_add_serial_to_components', 1),
('2016_10_27_213251_increase_serial_field_capacity', 1),
('2016_10_29_002724_enable_2fa_fields', 1),
('2016_10_29_082408_add_signature_to_acceptance', 1),
('2016_11_01_030818_fix_forgotten_filename_in_action_logs', 1),
('2016_11_13_020954_rename_component_serial_number_to_serial', 1),
('2016_11_16_172119_increase_purchase_cost_size', 1),
('2016_11_17_161317_longer_state_field_in_location', 1),
('2016_11_17_193706_add_model_number_to_accessories', 1),
('2016_11_24_160405_add_missing_target_type_to_logs_table', 1),
('2016_12_07_173720_increase_size_of_state_in_suppliers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `depreciation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `eol` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deprecated_mac_address` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fieldset_id` int(11) DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `requestable` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`id`, `name`, `model_number`, `manufacturer_id`, `category_id`, `created_at`, `updated_at`, `depreciation_id`, `user_id`, `eol`, `image`, `deprecated_mac_address`, `deleted_at`, `fieldset_id`, `notes`, `requestable`) VALUES
(1, 'ideapad', '234', 20, 1, '2019-03-28 10:20:57', '2019-03-28 10:20:57', NULL, 502, NULL, NULL, 0, NULL, NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('feri@docotel.com', '722901a5b52254830656df211e0c9e9ce47d57d46d79ea24670f2b91029f8ad7', '2017-09-25 13:31:14'),
('danang@docotel.com', 'f55a98721f1d3ea4eede78af91ce24e10a948558e5faba2a4971cfbb59c29218', '2017-11-02 07:52:03'),
('adityo@docotel.com', '5927d2f594ab12c030693726a0cec9290ac367587a8d068d9c505a2e78a1df6e', '2018-06-25 03:29:41'),
('annisa.tri@docotel.com', 'feb4a47d15494135eea90696e761a1ade2eaa1575de5c355aeb1f49f54fab599', '2019-01-15 03:04:12'),
('eigy@docotel.com', '808375846eb582397cef7a9b44efcd9fa6e1cf0b9bfc73329138ea61e9d3665a', '2019-03-08 01:54:31');

-- --------------------------------------------------------

--
-- Table structure for table `requested_assets`
--

CREATE TABLE `requested_assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `denied_at` datetime DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `request_code` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `per_page` int(11) NOT NULL DEFAULT '20',
  `site_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Snipe IT Asset Management',
  `qr_code` int(11) DEFAULT NULL,
  `qr_text` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_asset_name` int(11) DEFAULT NULL,
  `display_checkout_date` int(11) DEFAULT NULL,
  `display_eol` int(11) DEFAULT NULL,
  `auto_increment_assets` int(11) NOT NULL DEFAULT '0',
  `auto_increment_prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `load_remote` tinyint(1) NOT NULL DEFAULT '1',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alert_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alerts_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `default_eula_text` longtext COLLATE utf8_unicode_ci,
  `barcode_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'QRCODE',
  `slack_endpoint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slack_channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slack_botname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_currency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_css` text COLLATE utf8_unicode_ci,
  `brand` tinyint(4) NOT NULL DEFAULT '1',
  `ldap_enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_uname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_pword` longtext COLLATE utf8_unicode_ci,
  `ldap_basedn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_filter` text COLLATE utf8_unicode_ci,
  `ldap_username_field` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'samaccountname',
  `ldap_lname_field` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'sn',
  `ldap_fname_field` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'givenname',
  `ldap_auth_filter_query` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'uid=samaccountname',
  `ldap_version` int(11) DEFAULT '3',
  `ldap_active_flag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_emp_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_multiple_companies_support` tinyint(1) NOT NULL DEFAULT '0',
  `ldap_server_cert_ignore` tinyint(1) NOT NULL DEFAULT '0',
  `locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'en',
  `labels_per_page` tinyint(4) NOT NULL DEFAULT '30',
  `labels_width` decimal(6,5) NOT NULL DEFAULT '2.62500',
  `labels_height` decimal(6,5) NOT NULL DEFAULT '1.00000',
  `labels_pmargin_left` decimal(6,5) NOT NULL DEFAULT '0.21975',
  `labels_pmargin_right` decimal(6,5) NOT NULL DEFAULT '0.21975',
  `labels_pmargin_top` decimal(6,5) NOT NULL DEFAULT '0.50000',
  `labels_pmargin_bottom` decimal(6,5) NOT NULL DEFAULT '0.50000',
  `labels_display_bgutter` decimal(6,5) NOT NULL DEFAULT '0.07000',
  `labels_display_sgutter` decimal(6,5) NOT NULL DEFAULT '0.05000',
  `labels_fontsize` tinyint(4) NOT NULL DEFAULT '9',
  `labels_pagewidth` decimal(7,5) NOT NULL DEFAULT '8.50000',
  `labels_pageheight` decimal(7,5) NOT NULL DEFAULT '11.00000',
  `labels_display_name` tinyint(4) NOT NULL DEFAULT '0',
  `labels_display_serial` tinyint(4) NOT NULL DEFAULT '1',
  `labels_display_tag` tinyint(4) NOT NULL DEFAULT '1',
  `alt_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'C128',
  `alt_barcode_enabled` tinyint(1) DEFAULT '1',
  `alert_interval` int(11) DEFAULT '30',
  `alert_threshold` int(11) DEFAULT '5',
  `email_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_format` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'filastname',
  `username_format` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'filastname',
  `is_ad` tinyint(1) NOT NULL DEFAULT '0',
  `ad_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_port` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '389',
  `ldap_tls` tinyint(1) NOT NULL DEFAULT '0',
  `zerofill_count` int(11) NOT NULL DEFAULT '5',
  `ldap_pw_sync` tinyint(1) NOT NULL DEFAULT '1',
  `two_factor_enabled` tinyint(4) DEFAULT NULL,
  `require_accept_signature` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `user_id`, `per_page`, `site_name`, `qr_code`, `qr_text`, `display_asset_name`, `display_checkout_date`, `display_eol`, `auto_increment_assets`, `auto_increment_prefix`, `load_remote`, `logo`, `header_color`, `alert_email`, `alerts_enabled`, `default_eula_text`, `barcode_type`, `slack_endpoint`, `slack_channel`, `slack_botname`, `default_currency`, `custom_css`, `brand`, `ldap_enabled`, `ldap_server`, `ldap_uname`, `ldap_pword`, `ldap_basedn`, `ldap_filter`, `ldap_username_field`, `ldap_lname_field`, `ldap_fname_field`, `ldap_auth_filter_query`, `ldap_version`, `ldap_active_flag`, `ldap_emp_num`, `ldap_email`, `full_multiple_companies_support`, `ldap_server_cert_ignore`, `locale`, `labels_per_page`, `labels_width`, `labels_height`, `labels_pmargin_left`, `labels_pmargin_right`, `labels_pmargin_top`, `labels_pmargin_bottom`, `labels_display_bgutter`, `labels_display_sgutter`, `labels_fontsize`, `labels_pagewidth`, `labels_pageheight`, `labels_display_name`, `labels_display_serial`, `labels_display_tag`, `alt_barcode`, `alt_barcode_enabled`, `alert_interval`, `alert_threshold`, `email_domain`, `email_format`, `username_format`, `is_ad`, `ad_domain`, `ldap_port`, `ldap_tls`, `zerofill_count`, `ldap_pw_sync`, `two_factor_enabled`, `require_accept_signature`) VALUES
(1, '2017-08-23 11:06:57', '2019-03-28 09:59:56', 1, 20, 'Docotel Asset Management', 0, '', NULL, NULL, NULL, 1, '', 1, 'logo.png', '', 'feri@docotel.com', 1, '', 'QRCODE', '', '', '', 'Rp', '', 1, '0', '', 'glpi', 'eyJpdiI6Ilg4d01pdFNRVmpSSnpvUDhTUkFBS1Z6T09lUHpQSVptdVNoQ24xd2NYajg9IiwidmFsdWUiOiJEdlZFTEU4V2Q0SEtBXC9XeGtSM1wvdWZJc25Cb0ZMczZYajd2c3dHekMyZE09IiwibWFjIjoiMWIxMmQ2YjE3MjZlOWFkMGFlZGY5YTcwYjZlYWI4ZDBmZjUyOGI2NGFmNDRiYTQzYTkxMjkzMThjM2EzNTBiMiJ9', '', '', 'samaccountname', 'sn', 'givenname', 'uid=samaccountname', 3, '', '', '', 1, 0, 'en-ID', 30, '2.62500', '1.00000', '0.21975', '0.21975', '0.50000', '0.50000', '0.07000', '0.05000', 9, '8.50000', '11.00000', 1, 1, 1, 'C128', 1, 30, 5, 'docotel.com', 'firstname.lastname', 'filastname', 0, '', '389', 0, 6, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status_labels`
--

CREATE TABLE `status_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deployable` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text COLLATE utf8_unicode_ci,
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_in_nav` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status_labels`
--

INSERT INTO `status_labels` (`id`, `name`, `user_id`, `created_at`, `updated_at`, `deleted_at`, `deployable`, `pending`, `archived`, `notes`, `color`, `show_in_nav`) VALUES
(1, 'Pending', 1, NULL, NULL, NULL, 0, 1, 0, 'These assets are not yet ready to be deployed, usually because of configuration or waiting on parts.', NULL, 0),
(2, 'Ready to Deploy', 1, NULL, NULL, NULL, 1, 0, 0, 'These assets are ready to deploy.', NULL, 0),
(3, 'Archived', 1, NULL, NULL, NULL, 0, 0, 1, 'These assets are no longer in circulation or viable.', NULL, 0),
(318, 'Deployed', NULL, '2017-08-25 15:11:07', '2017-08-25 15:11:07', NULL, 1, 0, 0, NULL, NULL, 0),
(319, 'Installed', 1, '2017-09-11 05:42:42', '2017-09-11 06:29:36', NULL, 1, 0, 0, '', '#183dc6', 1),
(320, 'LG LED 19 in / 20M38', 147, '2017-09-11 16:13:41', '2017-09-12 04:39:53', '2017-09-12 04:39:53', 0, 1, 0, '', NULL, 0),
(321, 'Asus i3 X441UA-WX330T', 388, '2018-04-02 10:40:14', '2018-04-04 02:16:25', '2018-04-04 02:16:25', 1, 0, 0, '', NULL, 0),
(322, 'Lenovo 300-14IBK', 389, '2018-04-04 08:59:18', '2018-04-05 08:00:14', '2018-04-05 08:00:14', 1, 0, 0, '', NULL, 0),
(323, 'Dell W07B ALL IN ONE', 388, '2018-04-04 09:30:44', '2018-04-05 08:00:18', '2018-04-05 08:00:18', 1, 0, 0, '', NULL, 0),
(324, 'LG LED Monitor 16&quot;', 293, '2018-04-13 10:29:35', '2018-04-13 10:30:17', '2018-04-13 10:30:17', 1, 0, 0, '', NULL, 0),
(325, 'In Service', 293, '2018-04-16 08:40:48', '2018-04-16 08:43:09', NULL, 0, 1, 0, '', '', 0),
(326, 'Advertesing', 388, '2018-05-03 10:29:03', '2018-05-03 10:29:03', NULL, 1, 0, 0, '', NULL, 0),
(327, 'Broken ', 383, '2018-05-04 09:04:59', '2018-05-04 09:04:59', NULL, 0, 0, 1, '', '', 0),
(328, 'Dell Vostro S460', 388, '2018-06-05 03:55:58', '2019-01-04 02:35:23', '2019-01-04 02:35:23', 1, 0, 0, '', NULL, 0),
(329, 'Lenovo 310-14IKB', 383, '2018-07-02 02:49:43', '2019-01-04 02:35:27', '2019-01-04 02:35:27', 0, 0, 1, '', NULL, 0),
(330, 'Mining Machine', 549, '2018-08-31 06:53:00', '2019-01-04 02:35:31', '2019-01-04 02:35:31', 1, 0, 0, '', NULL, 0),
(331, 'Yudha Suci Widiawati', 163, '2018-09-04 07:49:27', '2019-01-04 02:35:35', '2019-01-04 02:35:35', 1, 0, 0, '', NULL, 0),
(332, 'Epson LX-300+II', 163, '2018-09-12 07:09:10', '2019-01-04 02:35:39', '2019-01-04 02:35:39', 1, 0, 0, '', NULL, 0),
(333, 'D-Link 24 Port switch / DGS-1024A', 390, '2019-02-14 09:56:09', '2019-02-28 07:57:10', '2019-02-28 07:57:10', 1, 0, 0, '', NULL, 0),
(334, 'Samsung UHD TV NU7100 ', 383, '2019-02-28 07:50:46', '2019-02-28 07:57:01', '2019-02-28 07:57:01', 1, 0, 0, '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `employee_num` text COLLATE utf8_unicode_ci,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` text COLLATE utf8_unicode_ci,
  `ldap_import` tinyint(1) NOT NULL DEFAULT '0',
  `locale` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'en',
  `show_in_list` tinyint(1) NOT NULL DEFAULT '1',
  `two_factor_secret` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_factor_enrolled` tinyint(1) NOT NULL DEFAULT '0',
  `two_factor_optin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`, `deleted_at`, `website`, `country`, `gravatar`, `location_id`, `phone`, `jobtitle`, `manager_id`, `employee_num`, `avatar`, `username`, `notes`, `company_id`, `remember_token`, `ldap_import`, `locale`, `show_in_list`, `two_factor_secret`, `two_factor_enrolled`, `two_factor_optin`) VALUES
(1, 'feri@docotel.com', '$2y$10$cBdMdv976kzhaVkXeoZUxegDVvyRQBN/VugFDmHo6Jhpw8Wx7zB4G', '{\"superuser\":\"1\",\"admin\":\"0\",\"reports.view\":\"0\",\"assets.view\":\"0\",\"assets.create\":\"0\",\"assets.edit\":\"0\",\"assets.delete\":\"0\",\"assets.checkin\":\"0\",\"assets.checkout\":\"0\",\"assets.view.requestable\":\"0\",\"accessories.view\":\"0\",\"accessories.create\":\"0\",\"accessories.edit\":\"0\",\"accessories.delete\":\"0\",\"accessories.checkout\":\"0\",\"accessories.checkin\":\"0\",\"consumables.view\":\"0\",\"consumables.create\":\"0\",\"consumables.edit\":\"0\",\"consumables.delete\":\"0\",\"consumables.checkout\":\"0\",\"licenses.view\":\"0\",\"licenses.create\":\"0\",\"licenses.edit\":\"0\",\"licenses.delete\":\"0\",\"licenses.checkout\":\"0\",\"licenses.keys\":\"0\",\"components.view\":\"0\",\"components.create\":\"0\",\"components.edit\":\"0\",\"components.delete\":\"0\",\"components.checkout\":\"0\",\"components.checkin\":\"0\",\"users.view\":\"0\",\"users.create\":\"0\",\"users.edit\":\"0\",\"users.delete\":\"0\",\"self.two_factor\":\"0\"}', 1, NULL, NULL, NULL, NULL, NULL, 'Feri', 'Harsanto', '2017-08-23 11:06:57', '2017-09-25 13:32:17', NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', NULL, 'feri', '', NULL, 'MKk5r3yYBVvkblnZoWh00aABQhuGKIAizdhNy1spgR7j1M8xryR7IuiTg8qG', 0, 'en', 1, NULL, 0, 0),
(502, 'super.admin@docotel.com', '$2y$10$GUQi/CdRTXXMaPr6DIIALuu.PZLQZP.WhFjIvzXWJHGj8cvZkFeVC', '{\"superuser\":\"0\",\"admin\":\"0\",\"reports.view\":\"0\",\"assets.view\":\"0\",\"assets.create\":\"0\",\"assets.edit\":\"0\",\"assets.delete\":\"0\",\"assets.checkin\":\"0\",\"assets.checkout\":\"0\",\"assets.view.requestable\":\"0\",\"accessories.view\":\"0\",\"accessories.create\":\"0\",\"accessories.edit\":\"0\",\"accessories.delete\":\"0\",\"accessories.checkout\":\"0\",\"accessories.checkin\":\"0\",\"consumables.view\":\"0\",\"consumables.create\":\"0\",\"consumables.edit\":\"0\",\"consumables.delete\":\"0\",\"consumables.checkout\":\"0\",\"licenses.view\":\"0\",\"licenses.create\":\"0\",\"licenses.edit\":\"0\",\"licenses.delete\":\"0\",\"licenses.checkout\":\"0\",\"licenses.keys\":\"0\",\"components.view\":\"0\",\"components.create\":\"0\",\"components.edit\":\"0\",\"components.delete\":\"0\",\"components.checkout\":\"0\",\"components.checkin\":\"0\",\"users.view\":\"0\",\"users.create\":\"0\",\"users.edit\":\"0\",\"users.delete\":\"0\",\"self.two_factor\":\"0\"}', 1, NULL, NULL, NULL, NULL, NULL, 'Super', 'Admin', '2018-04-20 10:00:12', '2019-03-28 10:11:52', NULL, '', NULL, '', 8, '', '', 222, '', NULL, 'super.admin', '', 1, 'HIz2j1zX76crf7B9VJE5faKDt99EYei6MmneP0pE2Vp7QNKQCUl6z7McCLne', 0, 'en', 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 2),
(147, 1),
(163, 1),
(165, 1),
(292, 1),
(293, 1),
(294, 1),
(295, 2),
(296, 1),
(375, 1),
(380, 1),
(383, 1),
(388, 1),
(389, 1),
(390, 1),
(502, 1),
(530, 1),
(549, 1),
(606, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessories`
--
ALTER TABLE `accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accessories_users`
--
ALTER TABLE `accessories_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `action_logs`
--
ALTER TABLE `action_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_logs_thread_id_index` (`thread_id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_logs`
--
ALTER TABLE `asset_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_maintenances`
--
ALTER TABLE `asset_maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_uploads`
--
ALTER TABLE `asset_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout_requests`
--
ALTER TABLE `checkout_requests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `checkout_requests_user_id_requestable_id_requestable_type_unique` (`user_id`,`requestable_id`,`requestable_type`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_name_unique` (`name`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components_assets`
--
ALTER TABLE `components_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumables`
--
ALTER TABLE `consumables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumables_users`
--
ALTER TABLE `consumables_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fieldsets`
--
ALTER TABLE `custom_fieldsets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depreciations`
--
ALTER TABLE `depreciations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `license_seats`
--
ALTER TABLE `license_seats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `requested_assets`
--
ALTER TABLE `requested_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_labels`
--
ALTER TABLE `status_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessories`
--
ALTER TABLE `accessories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accessories_users`
--
ALTER TABLE `accessories_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `action_logs`
--
ALTER TABLE `action_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asset_logs`
--
ALTER TABLE `asset_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_maintenances`
--
ALTER TABLE `asset_maintenances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_uploads`
--
ALTER TABLE `asset_uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `checkout_requests`
--
ALTER TABLE `checkout_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `components_assets`
--
ALTER TABLE `components_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `consumables`
--
ALTER TABLE `consumables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `consumables_users`
--
ALTER TABLE `consumables_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `custom_fieldsets`
--
ALTER TABLE `custom_fieldsets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `depreciations`
--
ALTER TABLE `depreciations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `license_seats`
--
ALTER TABLE `license_seats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `requested_assets`
--
ALTER TABLE `requested_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `status_labels`
--
ALTER TABLE `status_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=692;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
